TEMPLATE = app
QT += core network

TARGET = QtAudioStreamServer
CONFIG   += console
CONFIG   -= app_bundle

HEADERS       = server.h
SOURCES       = server.cpp \
                main.cpp

DESTDIR = bin
				
# install
INSTALLS += target
